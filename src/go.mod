module cubeutil

go 1.16

require (
	github.com/bwmarrin/discordgo v0.23.2
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/jcelliott/lumber v0.0.0-20160324203708-dd349441af25 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/schollz/jsonstore v1.1.0 // indirect
	github.com/sdomino/scribble v0.0.0-20200707180004-3cc68461d505
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83 // indirect
	golang.org/x/sys v0.0.0-20210301091718-77cc2087c03b // indirect
)
