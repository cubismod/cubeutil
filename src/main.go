package main

import (
	// "fmt"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"strings"

	// "strings"

	// "strconv"
	// "time"
	"github.com/bwmarrin/discordgo"
	"github.com/joho/godotenv"
	"github.com/sdomino/scribble"
	// "github.com/sdomino/scribble"
)

// Channel represents a channel in Discord
type Channel struct { Time string }

func main() {
	// create the bot
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	token := os.Getenv("TOKEN")
	dg, err := discordgo.New("Bot " + token)
	if err != nil {
		panic(err)
	}
	dg.AddHandler(ready)
	dg.AddHandler(messageCreate)

	err = dg.Open()
	if err != nil {
		fmt.Println("error opening connection", err)
		return
	}

	// Wait here until CTRL-C or other term signal is received.
	fmt.Println("Bot is now running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc
	
	// Cleanly close down the Discord session.
	dg.Close()
}

func ready(s *discordgo.Session, event *discordgo.Ready) {
	s.UpdateGameStatus(0, "u!")
	// get a reference to our json db
	db, err := scribble.New("../cfg/", nil)
	if err != nil {
		panic(err)
	}

	records, err := db.ReadAll("time")
	if err != nil {
		// probably means there are no channels added to the list yet so just return
		return
	}

	channels := []Channel{}
	for _, c := range records {
		channel := Channel{}
		if err := json.Unmarshal([]byte(c), &channel); err != nil {
			fmt.Printf("error", err)
		}
		channels = append(channels, channel)
	}
}

func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	db, err := scribble.New("../cfg/", nil)
	if err != nil {
		panic(err)
	}

	var prefix = "u!"
	var message = strings.ToLower(m.Content)
	var args = strings.Split(message, " ")
	if strings.HasPrefix(message, prefix) {
		// command handling
		var command = message[2:]
		switch command {
		case "ping":
			s.ChannelMessageSendReply(m.ChannelID, "pong", m.Reference())
		case "add":
			// add a channel
			if len(args) < 2 {
				s.ChannelMessageSendReply(m.ChannelID, "Syntax error, correct syntax is `u!add <channel_id> <timeout in min>`", m.Reference())
			} else {
				// check that the args are correct
				channel, err := s.Channel(args[1])
				if err != nil {
					fmt.Fprintln(os.Stderr, err)
					s.ChannelMessageSendReply(m.ChannelID, "unable to find that channel", m.Reference())
					return
				}
				
			}
		}
	}
}